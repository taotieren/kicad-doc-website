
require 'fileutils'
require 'yaml'
require 'asciidoctor'
require 'tempfile'

class Builder


	def initialize()
		@pageLookup = {}
		@generatedGuides = {}
		@outputLangHash = {}

		@definedVersions = YAML.load_file("_source/_data/versions.yml")
		@definedGuidePriorities = YAML.load_file("_source/_data/guide_priority.yml")
		@definedLanguages = YAML.load_file("_source/_data/language_definitions.yml")
		@definedTranslations = YAML.load_file("_source/_data/translations.yml")

		
		# preprocess to validate default
		@definedVersions.each do |version, versionInfo|
			if versionInfo['default']
				if @defaultVersion.nil?
					@defaultVersion = version
				else
					abort 'More than one default version found in the versions.yml! Only set default: true on one of them!'
				end
			end
		end

		@ignoreFolders = [
			'CMakeFiles',
			'gui_translation_howto',
			'Makefile',
			'cmake_install.cmake',
			'.',
			'..'
		]

	end

	def processVersion(version)
		seenLangs = []

		puts "##########"
		puts "Version #{version}"

		pathVersionBase = File.join('./kicad-doc-built', version)
		if !File.directory? pathVersionBase
			$stderr.puts "Directory does not exist #{pathVersionBase}!\n"
			return
		end

		if @generatedGuides[version].nil?
			@generatedGuides[version] = {}
		end

		seenLangs = []
		Dir.foreach(pathVersionBase) do |guideKey|
			next if @ignoreFolders.include?guideKey

			pathGuideBase = File.join(pathVersionBase, guideKey)
			next if !File.directory? pathGuideBase
			# do work on real items



			Dir.foreach(pathGuideBase) do |langKey|
				next if @ignoreFolders.include?langKey

				print "Processing #{guideKey} #{langKey}\n"
				pathGuideLangBase = File.join(pathGuideBase, langKey)
				next if !File.directory? pathGuideLangBase

				seenLangs.push(langKey)

				if @generatedGuides[version][langKey].nil?
					@generatedGuides[version][langKey] = []
				end

				# this is the "index" adoc file that includes all of them
				mainDocPath = File.join(pathGuideBase, langKey, guideKey+'.adoc')

				#load the asciidoc file...to get the translated title mainly
				doc = Asciidoctor.load_file mainDocPath

				
				pathOutputBase = File.join('./_source/', version, langKey, guideKey)
				FileUtils.mkdir_p(pathOutputBase) unless Dir.exist?(pathOutputBase)


				guide_files = Dir.glob(pathGuideLangBase + "/*").select{ |x| File.file? x }
				guide_image_files = Dir.glob(pathGuideLangBase + "/images/*").select{ |x| File.file? x }
				guide_icon_files = Dir.glob(pathGuideLangBase + "/images/icons/*").select{ |x| File.file? x }
				guide_lang_image_files = Dir.glob(pathGuideLangBase + "/images/"+langKey+"/*").select{ |x| File.file? x }


				FileUtils.cp(guide_files, pathOutputBase)

				FileUtils.mkdir_p(File.join(pathOutputBase, 'images', langKey))
				FileUtils.mkdir_p(File.join(pathOutputBase, 'images', 'icons'))
				FileUtils.cp(guide_image_files, File.join(pathOutputBase, 'images'))
				FileUtils.cp(guide_icon_files, File.join(pathOutputBase, 'images','icons'))
				FileUtils.cp(guide_lang_image_files, File.join(pathOutputBase, 'images', langKey))

				main_adoc_path = File.join(pathOutputBase, "#{guideKey}.adoc")
				#time to mangle files!

				baseUrl = "/%s/%s.html" % [guideKey,guideKey]

				description = ""
				if @definedTranslations["guide_descriptions"][langKey] != nil
					if @definedTranslations["guide_descriptions"][langKey][guideKey] != nil
						description = @definedTranslations["guide_descriptions"][langKey][guideKey]
					end
				end

				_updateMainAdoc(main_adoc_path,doc.doctitle,langKey, version, baseUrl, description)

				image_path = File.join('/img','guide-icons',guideKey+'.png')
				if !File.exist?(File.join("./_source/", image_path))
					image_path = '/img/guide-icons/placeholder.png'
				end

				epub_path = File.join('/',version, langKey, guideKey,guideKey+'.epub')
				if !File.exist?(File.join("./_source/", epub_path))
					epub_path = ''
				end

				pdf_path = File.join('/',version, langKey, guideKey,guideKey+'.pdf')
				if !File.exist?(File.join("./_source/", pdf_path))
					pdf_path = ''
				end


				priority = @definedGuidePriorities.index(guideKey)
				if priority.nil?
					priority = 99
				end

				expectedUrl = "/%s/%s/%s/%s.html" % [version, langKey, guideKey,guideKey]
				@generatedGuides[version][langKey].push({
										"title" => doc.doctitle, 
										"url" => expectedUrl,
										"image" => image_path,
										"description" => "",
										"pdf" => pdf_path,
										"epub" => epub_path,
										"priority" => priority
										})

				_addToPageIndex(baseUrl, langKey, version)
			end
		end


		# Lets create the selectable language list
		# seems silly but hey, just in case one disappears or appears, we need a friendly translation for the dropdown
		# and this enforces we have one

		seenLangs = seenLangs.uniq

		@definedLanguages.each do | lang, definition | 
			if seenLangs.include?(lang)
				@outputLangHash[lang] = definition
			else
				$stderr.puts "Unable to find definition of language '%s' in language_definitions.yml" % [lang]
			end
		end



		indexTemplate = File.read("_source/_templates/index.html")
		seenLangs.each do | lang |
			#write the language specific index

			_write_index_file(File.join("./_source/", version, lang, "index.html"), "Home", lang, version)
		end
		
		# write the "version index"
		_write_index_file(File.join("./_source/", version, "index.html"), "Home", "en", version)
	end



	def processCompiledDocs
		@definedVersions.each do |version, versionInfo|
			processVersion(version)
		end
		
		File.open("_source/_data/generated_guides.yml", "w") { |file| file.write(@generatedGuides.to_yaml) }
		File.open("_source/_data/generated_languages.yml", "w") { |file| file.write(@outputLangHash.to_yaml) }

		# Generate the main/true index which is really just the english one
		_write_index_file("./_source/index.html", "Home", "en", @defaultVersion)

		
		File.open("_source/_data/page_index.yml", "w") { |file| file.write(@pageLookup.to_yaml) }
	end


	def _addToPageIndex(path, lang, version)
		if @pageLookup[path].nil?
			@pageLookup[path] = {
				'versionIndex' => {},
				'langIndex' => {}
			}
		end
		
		if @pageLookup[path]['versionIndex'][version].nil?
			@pageLookup[path]['versionIndex'][version] = []
		end

		@pageLookup[path]['versionIndex'][version].push(lang)
		
		if @pageLookup[path]['langIndex'][lang].nil?
			@pageLookup[path]['langIndex'][lang] = []
		end

		@pageLookup[path]['langIndex'][lang].push(version)
	end

	def _write_index_file(path, title, lang, version)
		indexTemplate = File.read("_source/_templates/index.html")
		
		print "Writing index for " + lang + "\n"
		indexPage = indexTemplate
		indexPage = indexPage.gsub(/%%LANG%%/, lang)
		indexPage = indexPage.gsub(/%%VERSION%%/, version)

		File.open(path, "w") {|file| file.puts indexPage }
	end

	def _updateMainAdoc(adocPath, title, lang, version, baseurl, description)
		headerTemplate = File.read("_source/_templates/adoc_header.txt")
		headerTemplate = headerTemplate.gsub(/%%TITLE%%/, title)
		headerTemplate = headerTemplate.gsub(/%%LANG%%/, lang)
		headerTemplate = headerTemplate.gsub(/%%VERSION%%/, version)
		headerTemplate = headerTemplate.gsub(/%%BASEURL%%/, baseurl)
		headerTemplate = headerTemplate.gsub(/%%DESCRIPTION%%/, description)
		Tempfile.open File.basename(adocPath) do |tempfile|
			# prepend data to tempfile
			tempfile << headerTemplate
			tempfile.write "\n"

			File.open(adocPath, 'r+') do |file|
			# append original data to tempfile
			tempfile << file.read
			# reset file positions
			file.pos = tempfile.pos = 0
			# copy all data back to original file
			file << tempfile.read
			end
		end
	end
end