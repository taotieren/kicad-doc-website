# Expires map
map $sent_http_content_type $expires {
    default                    off;
    text/html                  epoch;
    ~image/                    2d;
    ~font/                     max;
    #css and js are fingerprinted so we can extend caching
    text/css                   max;
    application/javascript     max;
}

map $http_accept_language $langheader {
        default en;
        ~es es;
        ~fr fr;
        ~de de;
}

server {
    listen       8081;
    server_name docs.kicad-pcb.org;
    return 301 $scheme://docs.kicad.org$request_uri;
}

server {
    listen       8081 default_server;
    server_name  _;
    server_tokens off;

    # avoid port changing between container and external ports on redirects
    # there is also port_in_redirect but it things trickier in development when you aren't binding to port 80 :/
    absolute_redirect off;

    #charset koi8-r;
    #access_log  /var/log/nginx/log/host.access.log  main;

    gzip on;
    gzip_disable        "MSIE [1-6]\.";
    gzip_vary           on;

    gzip_comp_level 4;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types
        # text/html is always compressed by HttpGzipModule
        text/css
        text/javascript
        text/xml
        text/plain
        text/x-component
        application/javascript
        application/json
        application/xml
        application/rss+xml
        font/truetype
        font/opentype
        application/vnd.ms-fontobject
        image/svg+xml;

    expires $expires;

    error_page 404 /404.html;
    location = /404.html {
        root   /usr/share/nginx/html;
        internal;
    }

    # Route for openshift/kubernetes/docker healthchecks
    location /healthz {
        access_log off;
        default_type text/plain;
        return 200 "healthy\n";
    }

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    # explicit version rules need to be first to match first
    location ~* /6.99/(?<urltail>.*) {
        return 307 /master/$urltail;
    }

    location ~* /5.99/(?<urltail>.*) {
        return 307 /6.0/$urltail;
    }

    location ~* /5.0/(?<urltail>.*) {
        return 301 /5.1/$urltail;
    }
    
    location ~* /doxygen-python/(?<urltail>.*) {
        return 301 /doxygen-python-6.0/$urltail;
    }

    location ~* /(?<major>\d+).(?<minor>\d+).(?<patch>\*|\d+)/(?<urltail>.*) {
        return 301 /$major.$minor/$urltail;
    }

    location ~* /(?<major>\d+).(?<minor>\d+)/(?<lang>[^\/]+)/(?<app>[^\/]+).pdf$ {
        return 301 /$major.$minor/$lang/$app/$app.pdf;
    }
    
    location ~* /(?<version>(\d+\.\d+|master))/(?<lang>.+)/(?<app>.+)/$ {
        return 301 /$version/$lang/$app/$app.html;
    }

    # try to redirect unsupported languages to English
    location ~ /(?<version>(\d+\.\d+|master))/(?<lang>[a-z]+)/(?<urltail>.+) {
        root /usr/share/nginx/html;
        try_files /$version/$lang/$urltail /$version/en/$urltail =404;
    }

    location /kicad-doc-HEAD.tar.gz {
        return 301 https://kicad-downloads.s3.cern.ch/docs/kicad-doc-HEAD.tar.gz;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
